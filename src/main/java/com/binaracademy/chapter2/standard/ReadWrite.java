package com.binaracademy.chapter2.standard;

import com.binaracademy.chapter2.model.DataDiri;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.crypto.Data;
import jdk.nashorn.internal.runtime.arrays.ArrayIndex;

public class ReadWrite {

  public Map<String, DataDiri> mapDd = new HashMap<>();

  public void readFile(String fileLocation, String noKtp) {
    try{
      File file = new File(fileLocation); // ngambil detail file yg kita masukan lokasi file nya
      FileReader reader = new FileReader(file);
      BufferedReader br = new BufferedReader(reader);
      String line = "";
      String[] tempArr;
//      Map<String, DataDiri> mapDd = new HashMap<>();
      List<DataDiri> listDd = new ArrayList<>();
      while((line = br.readLine()) != null) {
        tempArr = line.split(",");
        for(int i = 1; i < tempArr.length; i++) {
          if(mapDd.get(tempArr[i])!=null) {
//            mapDd.put(tempArr[i], mapDd.get(tempArr[i])+1);
          }
        }
//        DataDiri dd = new DataDiri(tempArr[0], tempArr[1], tempArr[2], tempArr[3], tempArr[4]);
//        listDd.add(dd);
//        mapDd.put(tempArr[0], dd);
      }
      String line1 = "1234,Rizky,2010-15-15,Sunnyvale CA,0817276x";
      String[] line1Arr = {"1234", "Rizky", "2010-15-15", "Sunnivale CA", "0817276x"};
      br.close();

    } catch(IOException ioe) {
      ioe.printStackTrace();
    } catch(NullPointerException npe) {
      npe.printStackTrace();
    } catch (ArrayIndexOutOfBoundsException outOfBoundsException) {
      outOfBoundsException.printStackTrace();
    }
  }

  public void writeFile(String filePath)  {
    try {
      File file = new File(filePath);
      file.exists();
      if(file.createNewFile()) {
        System.out.println("New file has been created!");
      } else {
        System.out.println("Rewriting file in " + filePath);
      }
      FileWriter writer = new FileWriter(file);
      BufferedWriter bw = new BufferedWriter(writer);
      bw.write(dataDiri("3333"));
      bw.newLine();
      bw.newLine();
      bw.write(dataDiri("4444"));
      bw.flush();
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String dataDiri(String noKtp) {
    DataDiri detailDiri = new DataDiri();
    StringBuffer data = new StringBuffer("");
    if((detailDiri = mapDd.get(noKtp))!=null) {
      System.out.println("Hasil pencarian no KTP : " + noKtp);
      System.out.println("===== Data Diri ======");
      System.out.println("Nama : " + detailDiri.getNama());
      System.out.println("Alamat : " + detailDiri.getAlamat());
      System.out.println("No HP : " + detailDiri.getNoHp());
      System.out.println("Tgl Lahir : " + detailDiri.getTanggalLahir());
      data.append("Hasil pencarian no KTP : ").append(noKtp).append("\n")
          .append("===== Data Diri ======").append("Nama : ").append(detailDiri.getNama())
          .append("\n").append("Alamat : ").append(detailDiri.getAlamat()).append("\n")
          .append("No HP : ").append(detailDiri.getNoHp()).append("\n")
          .append("Tgl Lahir : ").append(detailDiri.getTanggalLahir());
    } else {
      System.out.println("no KTP : " + noKtp + " tidak ditemukan!!");
    }
    return data.toString();
  }

}
