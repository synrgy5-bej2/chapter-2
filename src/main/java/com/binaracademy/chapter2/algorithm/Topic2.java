package com.binaracademy.chapter2.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Topic2 {

  List<Integer> listData = Arrays.asList(2, 3, 4, 10, 40);

  public void search(int searchNum) {
    for(int i = 0; i < listData.size(); i++) {
      if (listData.get(i) == searchNum) {
        System.out.println("index of " + searchNum + " is " + i);
      }
    }
  }

}
