package com.binaracademy.chapter2;

import com.binaracademy.chapter2.algorithm.Topic2;
import com.binaracademy.chapter2.model.ContohLombok;
import com.binaracademy.chapter2.model.Manusia;
import com.binaracademy.chapter2.oop.Constantz;
import com.binaracademy.chapter2.oop.Kendaraan;
import com.binaracademy.chapter2.oop.Tronton;
import com.binaracademy.chapter2.oop.Truk;
import com.binaracademy.chapter2.standard.ReadWrite;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Main {

  public static void main(String[] args) {
//    String data1 = new String("rizky");
//    String data2 = new String("rizky");
//
//    if(data1.equals(data2)) {
//      System.out.println("Samaa!!");
//    } else {
//      System.out.println("Beda!");
//    }
//
//    String data3 = "ykzir";
//    String data4 = "ykzir";
//    if(data3 == data4) {
//      System.out.println("sama nich");
//    } else {
//      System.out.println("beda donk");
//    }
//
//    // contoh ngeprint yg tidak tepat, karena hanya print alamat memori dari object ybs aja
//    Manusia man = new Manusia();
//    System.out.println(man);

//    topic3();
//    topic4();
//    topic5();
//    topic6();
//    topic2();

    int n = 13;
    BigInteger big = new BigInteger("13");
    System.out.println(big.isProbablePrime(10));
//    System.out.println(BigInteger.probablePrime(n, new Random(n)));
  }

  public static void topic2() {
    Topic2 topic2 = new Topic2();
    topic2.search(10);
  }

  public static void topic3() {

    // instance object
    Manusia agus = new Manusia();
    agus.setTubuh("Ganteng");
    agus.setPikiran("Sadboi");

    System.out.println(agus.getPikiran());

    Manusia rizky = new Manusia("Rizky");

    Manusia.printNama();

//    Mobil teslaY = new Mobil();
//    teslaY.setMerk("Tesla");
//    teslaY.getMerk();

    ContohLombok contoh = new ContohLombok();
    contoh.setNama("Rizky");
    contoh.setUmur(17);
    ContohLombok contoh2 = new ContohLombok("contoh2", 40, "Unknown");
  }

  public static void topic4(){
    Kendaraan kendaraan = new Kendaraan() {
      @Override
      public int kapasitasPenumpang() {
        return 0;
      }

      @Override
      public int jumlahRoda() {
        return 0;
      }

      @Override
      public String platNomor() {
        return null;
      }

      @Override
      public String warna() {
        return null;
      }

      @Override
      public String merk() {
        return null;
      }
    };

//    Truk truk = new Truk() {
//      @Override
//      public double berat() {
//        return 0;
//      }
//    };

    Tronton tronton = new Tronton();
    tronton.isNyala = true;

    Kendaraan kendaraan1 = new Tronton();
    kendaraan1.isNyala = false;
    kendaraan1 = new Truk() {
      @Override
      public double berat() {
        return 0;
      }

      @Override
      public int jumlahRoda() {
        return 0;
      }
    };

    System.out.println(Constantz.PHI.getValue());
  }

  public static void topic5() {
    // array biasa / array tradisional
    System.out.println("Array Biasa");
    int[] intArray = new int[10];
    for(int i = 0; i < intArray.length; i++) {
      intArray[i] = i;
    }
    for(int i = 0; i < intArray.length; i++) {
      System.out.println(intArray[i]);
    }
    System.out.println("====");
    intArray[5] = 0;
    for(int i = 0; i < intArray.length; i++) {
      System.out.println(intArray[i]);
    }

    System.out.println("List");
    // collection / array termodernisasi
    List<Integer> listArray = new ArrayList<>();
    // tambah element berbentuk integer ke listArray
    for(int i = 0; i < 10; i++) {
      listArray.add(i);
    }

    for(int i = 0; i < listArray.size(); i++) {
      System.out.println(listArray.get(i));
    }
    System.out.println("======");

    listArray.remove(5);
    for(int i = 0; i < listArray.size(); i++) {
      System.out.println(listArray.get(i));
    }

    System.out.println("===== add di index tertentu =====");
    listArray.add(3, 777);
    for(int i = 0; i < listArray.size(); i++) {
      System.out.println(listArray.get(i));
    }

//    System.out.println("===== add di index yg jauh =====");
//    listArray.add(50, 90);
//    for(int i = 0; i < listArray.size(); i++) {
//      System.out.println(listArray.get(i));
//    }

    listArray.set(0, 1000);
    System.out.println(listArray.get(0));

    /**
     * 4 3 2    list 1
     * 5 1 1    list 2
     * 9 7 8    list 3
     *
     * list 1, 2, dan 3 dikumpulin jadi 1 list yg berupa 2d
     * [[4,3,2],[5,1,1],[9,7,8]]
     */

    System.out.println("list di dalam list");
    List<List<Integer>> listOfLists = new ArrayList<>();
    for(int i = 0; i < 3; i++) {
      List<Integer> listDalam = new ArrayList<>();
      for(int j = 0; j < 3; j++) {
        listDalam.add(j);
      }
      listOfLists.add(listDalam);
    }
    Integer[] intArrayBiasa = new Integer[10];
    List<Integer> arrToCol = Arrays.asList(intArrayBiasa);

    System.out.println("for each dengan contoh perabot");
    /**
     * Kursi, Meja, Lemari, Ember, Gayung, Kursi, Gayung
     * String[] perabot = {"Kursi", "Meja", ...}
     */
    List<String> perabot = Arrays.asList("Kursi", "Meja", "Lemari", "Ember", "Gayung", "Kursi", "Gayung");
    for(String perabot1 : perabot) {
      System.out.println(perabot1);
    }

    System.out.println("ini klo pake set");
    Set<String> perabotSet = new HashSet<>();
    perabotSet.addAll(perabot);
    for(String set1 : perabotSet) {
      System.out.println(set1);
    }

    System.out.println("Pake Map");
    Map<String, String> alamat = new HashMap<>();
    alamat.put("Pak Rizky", "10, 10");
    alamat.put("Bu Besty", "12, 50");
    alamat.put("Pak Agus", "58, 40");
    alamat.put("Kak Nadiem", "10, 10");
    System.out.println(alamat.get("Pak Rizky"));

    List<String> rumahYgSamaan = new ArrayList<>();
    Set<Map.Entry<String, String>> entries = alamat.entrySet();
    for(Map.Entry<String, String> element : entries) {
      if(element.getValue().equals("10, 10")) {
        rumahYgSamaan.add(element.getKey());
      }
    }
    System.out.println("---- rumah yg samaan logic ----");
    for (String pemilik: rumahYgSamaan) {
      System.out.println(pemilik);
    }

    List<Map<String, String>> datas = new ArrayList<>();

    Map<String, List<String>> data5 = new HashMap<>();
  }

  public static void topic6() {
    ReadWrite rw = new ReadWrite();
    rw.readFile("/Users/hf-user/Desktop/work/Binar/Chapter-2/src/main/resources/dataDiri.csv",
        "1234");
    rw.writeFile("/Users/hf-user/Desktop/work/Binar/Chapter-2/src/main/resources/detail-target.txt");
  }

}
