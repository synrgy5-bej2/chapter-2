package com.binaracademy.chapter2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContohLombok {

  private String nama;
  private int umur;
  private String jenisKelamin;

}
