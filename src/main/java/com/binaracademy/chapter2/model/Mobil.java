package com.binaracademy.chapter2.model;

import java.util.Arrays;

public class Mobil {

  private String merk;
  private String model;
  private String warna;
  public static final int JUMLAH_BAN = 4;

  public String getMerk() {
    return merk;
  }

  protected void setMerk(String merk) {
    this.merk = merk;
  }

  public String getModel() {
    return model;
  }

  protected void setModel(String model) {
    this.model = model;
  }

  public String getWarna() {
    return warna;
  }

  protected void setWarna(String warna) {
    this.warna = warna;
  }

  // Method overload
  public void startMesin() {
    System.out.println("mesin di starter");
  }

  public void startMesin(String lokasiStart) {
    System.out.println("mesin di starter di " + lokasiStart);
  }

  public void startMesin(int test) {

  }

  public void startMesin(String lokasiStart, String lokasiTujuan) {
    System.out.println("mesin di starter di " + lokasiStart + " untuk otw ke " + lokasiTujuan);
  }

  public void startMesin(String supir, int penumpang) {

  }


}
