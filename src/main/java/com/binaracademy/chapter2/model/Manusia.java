package com.binaracademy.chapter2.model;

public class Manusia {

  private String tubuh;
  private String pikiran;

  // Constructor
  public Manusia() {
    System.out.println("Kenalan deh");

    Mobil teslaY = new Mobil();
    teslaY.setMerk("Tesla");
    teslaY.setModel("Y");
    teslaY.setWarna("Merah");
  }

  public Manusia(String namaku) {
    System.out.println("Halo namaku : " + namaku);
  }

  // Method static
  public static void printNama() {
    System.out.println("Nama default");
  }

  // getter setter
  public String getTubuh() {
    return tubuh;
  }

  public void setTubuh(String tubuh) {
    this.tubuh = tubuh;
  }

  public String getPikiran() {
    return pikiran;
  }

  public void setPikiran(String pikiran) {
    this.pikiran = pikiran;
  }

}
