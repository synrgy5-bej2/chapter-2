package com.binaracademy.chapter2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data // udah include @Setter dan @Getter
@AllArgsConstructor
@NoArgsConstructor
public class DataDiri {

  private String noKtp;
  private String nama;
  private String tanggalLahir;
  private String alamat;
  private String noHp;

}
