package com.binaracademy.chapter2.service;

public interface IOFileService {

  void readFile();
  void writeFile();

}
