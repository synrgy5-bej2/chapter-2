package com.binaracademy.chapter2.oop;

public class StaticFinal {

  public void contohMethod() {
    // inner class yg pake static
    InnerClassStatic.methodPrint();

    // inner class tanpa static
    InnerClass innerClass = new InnerClass();
    innerClass.methodBeep();
  }

  public static class InnerClassStatic {
    public static void methodPrint() {
      System.out.println("Ping!");
    }
  }

  public class InnerClass {
    public void methodBeep() {
      System.out.println("beep");
    }
  }

}
