package com.binaracademy.chapter2.oop;

// final class : gabisa punya anak/subclass, properties berhenti di class Jazz
//public final class Jazz implements Mobil {

public class Jazz implements Mobil {

  static final String model = "Jazz";

  public Jazz() {
    // final variable tidak bisa di modify
//    MERK_MOBIL = "Mio";
  }

  @Override
  public void startMesin() {
    System.out.println(MERK_MOBIL + " Mesin sudah nyala");
  }

  @Override
  public final String supir() {
    return "Rizky";
  }
}
