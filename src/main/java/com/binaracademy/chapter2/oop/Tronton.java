package com.binaracademy.chapter2.oop;

public class Tronton extends Truk {

  public Tronton() {
    // cara modify value field induk nya.
    isNyala = false;
    System.out.println("Tronton menyala : " + isNyala);
    System.out.println("Jumlah Roda Tronton : " + jumlahRoda());
    System.out.println("Berat tronton : " + berat());
  }

  @Override
  public int jumlahRoda() {
    return 10;
  }

  @Override
  public double berat() {
    return 1000;
  }

  @Override
  public String warna() {
    return "Hijau";
  }

  public String warna(String warnanya) {
    return "Hijau tapi Biru";
  }

}
