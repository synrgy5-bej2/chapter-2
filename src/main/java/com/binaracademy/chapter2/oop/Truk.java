package com.binaracademy.chapter2.oop;

// extends max 1 abstract class, implements bisa banyak interface
public abstract class Truk extends Kendaraan implements Mobil, GayaHidup {

  public abstract double berat();

  public Truk() {
    System.out.println("Truk nyala : " + isNyala);
    System.out.println("Kapasitas Penumpang : " + kapasitasPenumpang());
    System.out.println("Jumlah Roda : " + jumlahRoda());
  }

  @Override
  public int kapasitasPenumpang() {
    return 3;
  }

  public abstract int jumlahRoda();

  @Override
  public String platNomor() {
    return "B 12345 AHOY";
  }

  @Override
  public String warna() {
    return "Kuning";
  }

  @Override
  public String merk() {
    return "Daihatsu";
  }

  // implementasi method abstract dari interface Mobil
  @Override
  public void startMesin() {
    System.out.println("Start Mesin Truk!");
  }

  @Override
  public String supir() {
    return "Akoe";
  }
}
