package com.binaracademy.chapter2.oop;

import lombok.Getter;

@Getter
public enum Constantz {
  PHI(3.14),
  HARI_SEMINGGU(7),
  BULAN_SETAHUN(12);

  private double value;
  private Constantz(double value){
    this.value = value;
  }
}
