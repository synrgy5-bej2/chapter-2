package com.binaracademy.chapter2.oop;

public interface Mobil {

  // static final ga perlu ditulis, karena udah otomatis dalam interface
  // static final = constant
  static final String MERK_MOBIL = "Honda";
  String WARNA = "HITAM ATAU PUTIH";

  public abstract void startMesin();
  String supir();

  default void test() {

  }
}
